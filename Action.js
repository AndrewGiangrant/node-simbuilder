var _ = require('underscore');
var async = require('async');
module.exports = Action;

function Action(type, options) {
    var self = this;
    switch(type.toLowerCase()) {
        case "answer":
            self.type = "AnswerAction";
            self.inverse = false;
        break;
        case "verifyphonegoesoffhook":
            self.type = "VerifyPhoneGoesOffhookAction";
            self.inverse = false;
            self.timeoutMilliseconds = 10000;
        break;
        case "syncpoint":
            self.type = "SyncPointAction";
            self.inverse = false;
            self.timeoutMilliseconds = 60000;
            self.syncPointName = "use setSyncPointName()";
            self.waitForUserAfterAllInteractionsReachSyncPoint = false;
        break;
        case "playfile":
            self.type = "PlayFileAction";
            self.waitForCompletion = true;
            self.filePath = "use setFilePath()";
        break;
        case "spotkeyword":
            self.type = "SpotKeywordAction";
            self.inverse = false;
            self.timeoutMilliseconds = 30000;
        break;
        case "notify":
            self.type = "NotifyAction";
            self.notifyText = "use setNotifyText()";
        break;
        case "pause":
            self.type = "PauseAction";
            self.pauseTimeMilliseconds = 5000;
        break;
        case "gettone":
            self.type = "GetToneAction";
            self.inverse = false;
            self.timeoutMilliseconds = 30000;
            self.freq1 = 1000;
        break;
        case "initiateinteraction":
            self.type = "InitiateInteractionAction";
            self.inverse = false;
            self.timeoutMilliseconds = 600000;
            self.waitForCompletion = false;
        break;
        default:
        break;
    }
    if(_.isObject(options)) {
        async.forEachOf(options, function(value, key, callback) {
            self[key] = value;
            callback();
        });
    }
}

Action.prototype.setType = function(type) {
    var self = this;
    self.type = type;
    return self;
};

Action.prototype.requireAutoAnswer = function(requireb) {
    var self = this;
    self.requireAutoAnswer = requireb;
    return self;
};

Action.prototype.setInverse = function(inverseb) {
    var self = this;
    self.inverse = inverseb;
    return self;
};

Action.prototype.setTimeout = function(timeoutMilliseconds) {
    var self = this;
    self.timeoutMilliseconds = timeoutMilliseconds;
    return self;
};

Action.prototype.setEnabled = function(enabledb) {
    var self = this;
    self.enabled = enabledb;
    return self;
};

Action.prototype.setWaitForCompletion = function(waitb) {
    var self = this;
    self.waitForCompletion = waitb;
    return self;
};

Action.prototype.setSyncPointName = function(name) {
    var self = this;
    self.syncPointName = name;
    return self;
};

Action.prototype.setWaitForUser = function(waitb) {
    var self = this;
    self.waitForUserAfterAllInteractionsReachSyncPoint = waitb;
    return self;
};

Action.prototype.setFilePath = function(filePath) {
    var self = this;
    self.filePath = filePath;
    return self;
};

Action.prototype.addKeyword = function(keyword) {
    var self = this;
    if(_.isUndefined(self.keywords))
        self.keywords = [];
    if(_.isArray(self.keywords))
        self.keywords.push(keyword);
    return self;
};

Action.prototype.addKeywords = function(keywords) {
    var self = this;
    if(_.isUndefined(self.keywords))
        self.keywords = [];
    if(_.isArray(self.keywords))
        self.keywords.concat(keywords);
    return self;
};

Action.prototype.setKeyword = function(keyword) {
    var self = this;
    self.keywords = [keyword];
    return self;
};

Action.prototype.setKeywords = function(keywords) {
    var self = this;
    self.keywords = keywords;
    return self;
};

Action.prototype.addUnexpectedKeyword = function(unexpectedKeyword) {
    var self = this;
    if(_.isUndefined(self.unexpectedKeywords))
        self.unexpectedKeywords = [];
    if(_.isArray(self.unexpectedKeywords))
        self.unexpectedKeywords.push(unexpectedKeyword);
    return self;
};

Action.prototype.addUnexpectedKeywords = function(unexpectedKeywords) {
    var self = this;
    if(_.isUndefined(self.unexpectedKeywords))
        self.unexpectedKeywords = [];
    if(_.isArray(self.unexpectedKeywords))
        self.unexpectedKeywords.concat(unexpectedKeywords);
    return self;
};

Action.prototype.setUnexpectedKeyword = function(unexpectedKeyword) {
    var self = this;
    self.unexpectedKeywords = [unexpectedKeyword];
    return self;
};

Action.prototype.setUnexpectedKeywords = function(unexpectedKeywords) {
    var self = this;
    self.unexpectedKeywords = unexpectedKeywords;
    return self;
};

Action.prototype.setPauseTime = function(timeMilliseconds) {
    var self = this;
    self.pauseTimeMilliseconds = timeMilliseconds;
    return self;
};

Action.prototype.setFreq = function(freq) {
    var self = this;
    self.freq1 = freq;
    return self;
};

Action.prototype.setNotifyText = function(text) {
    var self = this;
    self.notifyText = text;
    return self;
};

Action.prototype.setScenario = function(scenario) {
    var self = this;
    self.scenario = scenario.scenario;
    return self;
};