var _ = require('underscore');
var async = require('async');
module.exports = Scenario;

function Scenario(type, name, options) {
    var self = this;
    self.type = "PeriodicBurstSchedule";
    self.maxInteractions = 1;
    self.numInteractions = 1;
    self.burstIntervalMilliseconds = 1000;
    self.scenario = {
        "scenarioName": name,
        "sendDebugNotifications": true,
        "disconnectOnActionFailure": true,
        "scoreInboundAudio": false,
        "atConclusion": "Disconnect",
        "tos": [],
        "froms": [],
        "actions": []
    };
    switch(type) {
        case "inbound":
            self.scenario.type = "InboundCallScenario";
            self.scenario.inboundCallTimeoutMilliseconds = 60000;
        break;
        case "outbound":
            self.scenario.type = "OutboundCallScenario";
            self.scenario.startEvent = "Connect";
            self.scenario.sendAttributesAsAai = false;
            self.startTimeMilliseconds = 1000;
        break;
        default:
        break;
    }
    if(_.isObject(options)) {
        async.forEachOf(options, function(value, key, callback) {
            if(key === "scenario") {
                async.forEachOf(options[key], function(value, key, callback) {
                    self.scenario[key] = value;
                    callback();
                });
            }
            else
                self[key] = value;
            callback();
        });
    }
}

Scenario.prototype.setType = function(type) {
    var self = this;
    self.type = type;
    return self;
};

Scenario.prototype.setMaxInteractions = function(maxNum) {
    var self = this;
    self.maxInteractions = maxNum;
    return self;
};

Scenario.prototype.setNumInteractions = function(num) {
    var self = this;
    self.numInteractions = num;
    return self;
};

Scenario.prototype.setBurstInterval = function(milliseconds) {
    var self = this;
    self.burstIntervalMilliseconds = milliseconds;
    return self;
};

Scenario.prototype.setScenarioType = function(type) {
    var self = this;
    self.scenario.type = type;
    return self;
};

Scenario.prototype.setScenarioName = function(name) {
    var self = this;
    self.scenario.scenarioName = name;
    return self;
};

Scenario.prototype.setSendDebug = function(sendb) {
    var self = this;
    self.scenario.sendDebugNotifications = sendb;
    return self;
};

Scenario.prototype.setDisconnectOnFailure = function(disconnectb) {
    var self = this;
    self.scenario.disconnectOnActionFailure = disconnectb;
    return self;
};

Scenario.prototype.setScoreAudio = function(scoreb) {
    var self = this;
    self.scenario.scoreInboundAudio = scoreb;
    return self;
};

Scenario.prototype.setConclusion = function(conclusion) {
    var self = this;
    self.scenario.atConclusion = conclusion;
    return self;
};

Scenario.prototype.setTos = function(tos) {
    var self = this;
    self.scenario.tos = tos;
    return self;
};

Scenario.prototype.addTos = function(tos) {
    var self = this;
    self.scenario.tos.concat(tos);
    return self;
};

Scenario.prototype.setTo = function(to) {
    var self = this;
    self.scenario.tos = [to];
    return self;
};

Scenario.prototype.addTo = function(to) {
    var self = this;
    self.scenario.tos.push(to);
    return self;
};

Scenario.prototype.setFroms = function(froms) {
    var self = this;
    self.scenario.froms = froms;
    return self;
};

Scenario.prototype.addFroms = function(froms) {
    var self = this;
    self.scenario.froms.concat(froms);
    return self;
};

Scenario.prototype.setFrom = function(from) {
    var self = this;
    self.scenario.froms = [from];
    return self;
};

Scenario.prototype.addFrom = function(from) {
    var self = this;
    self.scenario.froms.push(from);
    return self;
};

Scenario.prototype.setActions = function(actions) {
    var self = this;
    self.scenario.actions = actions;
    return self;
};

Scenario.prototype.addActions = function(actions) {
    var self = this;
    self.scenario.actions = self.scenario.actions.concat(actions);
    return self;
};

Scenario.prototype.setAction = function(action) {
    var self = this;
    self.scenario.actions = [action];
    return self;
};

Scenario.prototype.addAction = function(action) {
    var self = this;
    self.scenario.actions.push(action);
    return self;
};

Scenario.prototype.setInboundTimeout = function(milliseconds) {
    var self = this;
    self.scenario.inboundCallTimeoutMilliseconds = milliseconds;
    return self;
};

Scenario.prototype.setStartEvent = function(startEvent) {
    var self = this;
    self.scenario.startEvent = startEvent;
    return self;
};

Scenario.prototype.setStartTime = function(milliseconds) {
    var self = this;
    self.startTimeMilliseconds = milliseconds;
    return self;
};

Scenario.prototype.setWaitForDCTimeout = function(milliseconds) {
    var self = this;
    self.scenario.waitForDisconnectTimeoutMilliseconds = milliseconds;
    return self;
};