var Scenario = require('./Scenario');
var Action = require('./Action');
var _ = require('underscore');
var async = require('async');
module.exports = {
    Simulation: simulation,
    Action: Action,
    Scenario: Scenario
};

function simulation(server, options) {
    var self = this;
    self.type = "Simulation";
    self.bISVersion = "3.1.5273.22649";
    self.stopOnInteractionFailure = true;
    self.globalICServers = [{"iCServer": server}];
    self.schedules = [];
    if(_.isObject(options)) {
        async.forEachOf(options, function(value, key, callback) {
            self[key] = value;
            callback();
        });
    }
}

simulation.prototype.setBISVersion = function(version) {
	var self = this;
	self.bISVersion = version;
    return self;
};

simulation.prototype.setStopOnFailure = function(stopb) {
	var self = this;
	self.stopOnInteractionFailure = stopb;
    return self;
};

simulation.prototype.addScenarios = function(scenarioArray) {
	var self = this;
    self.schedules = self.schedules.concat(scenarioArray);
    return self;
};

simulation.prototype.addScenario = function(scenario) {
    var self = this;
    self.schedules.push(scenario);
    return self;
};

simulation.prototype.setScenarios = function(scenarioArray) {
    var self = this;
    self.schedules = scenarioArray;
    return self;
};

simulation.prototype.setScenario = function(scenario) {
    var self = this;
    self.schedules = [scenario];
    return self;
};

module.exports.verify2WayAudio = function(file1, keyword1, file2, keyword2, optionalPrefix) {
    if(_.isUndefined(optionalPrefix))
        optionalPrefix = "";
    var scenario1ActionsArray = [];
    var scenario2ActionsArray = [];
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint1"}));
    scenario1ActionsArray.push(new Action("playfile", {"filePath": file1}));
    scenario1ActionsArray.push(new Action("playfile", {"filePath": file1}));
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint2"}));
    scenario1ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword2]}));
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint3"}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint1"}));
    scenario2ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword1]}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint2"}));
    scenario2ActionsArray.push(new Action("playfile", {"filePath": file2}));
    scenario2ActionsArray.push(new Action("playfile", {"filePath": file2}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint3"}));
    var scenarioArray = [scenario1ActionsArray, scenario2ActionsArray];
    return scenarioArray;
};

module.exports.verify3WayAudio = function(file1, keyword1, file2, keyword2, file3, keyword3, optionalPrefix) {
    if(_.isUndefined(optionalPrefix))
        optionalPrefix = "";
    var scenario1ActionsArray = [];
    var scenario2ActionsArray = [];
    var scenario3ActionsArray = [];
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint1"}));
    scenario1ActionsArray.push(new Action("playfile", {"filePath": file1}));
    scenario1ActionsArray.push(new Action("playfile", {"filePath": file1}));
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint2"}));
    scenario1ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword2]}));
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint3"}));
    scenario1ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword3]}));
    scenario1ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint4"}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint1"}));
    scenario2ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword1]}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint2"}));
    scenario2ActionsArray.push(new Action("playfile", {"filePath": file2}));
    scenario2ActionsArray.push(new Action("playfile", {"filePath": file2}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint3"}));
    scenario2ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword3]}));
    scenario2ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint4"}));
    scenario3ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint1"}));
    scenario3ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword1]}));
    scenario3ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint2"}));
    scenario3ActionsArray.push(new Action("spotkeyword", {"keywords": [keyword2]}));
    scenario3ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint3"}));
    scenario3ActionsArray.push(new Action("playfile", {"filePath": file3}));
    scenario3ActionsArray.push(new Action("playfile", {"filePath": file3}));
    scenario3ActionsArray.push(new Action("syncpoint", {"syncPointName": optionalPrefix+"SyncPoint4"}));
    var scenarioArray = [scenario1ActionsArray, scenario2ActionsArray, scenario3ActionsArray];
    return scenarioArray;
};